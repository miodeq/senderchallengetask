#!/bin/sh

set -e

NUM_WORKERS=3
NUM_THREADS=3

./wait-for-it.sh postgres:5432

echo "${0}: running migrations."
poetry run python manage.py migrate

echo "${0}: collecting statics."
poetry run python manage.py collectstatic --no-input

echo "${0}: creating admin user."
echo "from django.contrib.auth.models import User; print(\"Admin exists\") if User.objects.filter(username='$DJANGO_USERNAME').exists() else User.objects.create_superuser(username='$DJANGO_USERNAME', password='$DJANGO_PASSWORD')" | poetry run python manage.py shell

echo "${0}: running server."
poetry run gunicorn sender_challenge.wsgi:application \
    --name=root \
    --bind=0.0.0.0:8000 \
    --timeout=900 \
    --workers=$NUM_WORKERS \
    --threads=$NUM_THREADS \
    --reload \
    --pid=/src/sender_challenge.pid

exec "$@"
