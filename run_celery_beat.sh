#!/bin/sh

set -e

sleep 5

poetry run celery -A sender_challenge.celery beat -l info

exec "$@"
