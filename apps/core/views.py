import sys

from django.shortcuts import render
from django.views.generic import ListView

from apps.core.models import Movie
from apps.core.utils import init_logger

logger = init_logger()


class MoviesView(ListView):
    """
    Returns all movies data.
    """

    model = Movie
    template_name = "movies_list.html"
    context_object_name = "movies"


def handler403(request, exception):
    """
    Custom 403 handler.
    """

    logger.error(msg=str(exception))

    context = {}

    return render(request=request, template_name="errors/403.html", status=403, context=context)


def handler404(request, exception):
    """
    Custom 404 handler.
    """

    logger.error(msg=str(exception))

    context = {}

    return render(request=request, template_name="errors/404.html", status=404, context=context)


def handler500(request):
    """
    Custom 500 handler.
    """

    _, value, _ = sys.exc_info()

    logger.error(msg=value)

    context = {}

    return render(request=request, template_name="errors/500.html", status=500, context=context)
