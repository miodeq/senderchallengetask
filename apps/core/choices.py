from enum import Enum


class BaseEnum(Enum):
    @classmethod
    def choices(cls):
        return [(key.name, key.value) for key in cls]

    @classmethod
    def allowed_choices(cls):
        return {key.name for key in cls}

    @classmethod
    def to_json(cls):
        return {key.name: key.value for key in cls}


class PersonGenderChoices(BaseEnum):
    MALE = "male"
    FEMALE = "female"
    NA = "na"


class EyeColorChoices(BaseEnum):
    BLACK = "black"
    BROWN = "brown"
    DARK_BROWN_BLACK = "dark brown/black"
    EMERALD = "emerald"
    GREEN = "green"
    HAZEL = "hazel"
    RED = "red"
    WHITE = "white"
    YELLOW = "yellow"
    GREY = "grey"
    BLUE = "blue"


class HairColorChoices(BaseEnum):
    BALD = "bald, but beard is brown"
    BLACK = "black"
    BLUE = "blue"
    BROWN = "brown"
    DARK_BROWN = "dark brown"
    GREY = "grey"
    BEIGE = "beige"
    LIGHT_BROWN = "light brown"
    LIGHT_ORANGE = "light orange"
    WHITE = "white"
    YELLOW = "yellow"
