from django.contrib import admin

from apps.core.models import Movie, Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    """
    PersonAdmin admin interface.
    """

    list_display = (
        "person_id",
        "name",
        "gender",
        "age",
        "eye_color",
        "hair_color",
    )

    list_filter = ("gender", "dt_created", "dt_modified")

    readonly_fields = ("dt_modified", "dt_created", "person_id")

    ordering = ("-age",)

    search_fields = ("name", "person_id", "eye_color", "hair_color", "producer")

    list_per_page = 20


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    """
    MovieAdmin admin interface.
    """

    list_display = (
        "movie_id",
        "title",
        "director",
        "producer",
        "release_dt",
        "rt_score",
        "get_movie_people",
    )

    raw_id_fields = ("people",)

    list_filter = ("release_dt", "dt_created", "dt_modified")

    readonly_fields = ("dt_modified", "dt_created", "movie_id")

    ordering = ("-rt_score", "-release_dt")

    search_fields = ("movie_id", "title", "description", "director", "producer")

    list_per_page = 20
