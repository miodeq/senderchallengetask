import logging
import sys

import requests
from django.conf import settings
from user_agent import generate_user_agent


def merge_movie_with_people():
    """
    Finds relation between movie-people and returns movies data.
    """

    with requests.Session() as session:
        session.headers.update(**{"User-Agent": generate_user_agent()})

        movies_data = session.get(url=settings.GHIBLI_FILMS_ENDPOINT).json()
        people_data = session.get(url=settings.GHIBLI_PEOPLE_ENDPOINT).json()

        data = {}

        for item in people_data:
            for movie in item.get("films"):
                movie_id = movie.split(settings.GHIBLI_FILMS_ENDPOINT)[1]

                if movie_id not in data:
                    data[movie_id] = [item]
                else:
                    data[movie_id].append(item)

        for movie in movies_data:
            movie["people"] = data.get(movie.get("id"))

        return movies_data


def init_logger(name=None, level=None):
    """
    Basic logger with timestamp.
    """

    logger = logging.getLogger(name=name or "[SenderChallengeTask]")

    log_format = "%(asctime)-15s %(levelname)s %(name)s %(message)s"

    formatter = logging.Formatter(log_format)

    handler_stream = logging.StreamHandler(sys.stdout)
    handler_stream.setFormatter(formatter)

    logger.addHandler(handler_stream)

    logger.setLevel(level=level or logging.INFO)

    return logger
