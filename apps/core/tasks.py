from celery import shared_task

from apps.core.choices import PersonGenderChoices, EyeColorChoices, HairColorChoices
from apps.core.utils import init_logger, merge_movie_with_people

logger = init_logger()


@shared_task(
    name="collect_movies_data_task",
    queue="api_worker",
    max_retries=5,
    bind=True,
    ignore_result=True,
    acks_late=True,
)
def collect_movies_data_task(self):
    """
    Celery tasks that handles retrieving movies data from API.
    """

    from apps.core.models import Movie, Person

    logger.info(msg="Preparing to make request to Ghibli API.")

    try:
        send_statuses = {}

        # movies data from API
        movies_data = merge_movie_with_people()

        logger.info(msg=f"Movies data: {len(movies_data)}")

        # the fastest would be bulk_create, but we need to do relations between Movie<->Person
        for movie_item in movies_data:
            instance, created = Movie.objects.update_or_create(
                movie_id=movie_item.get("id"),
                title=movie_item.get("title"),
                description=movie_item.get("description"),
                director=movie_item.get("director"),
                producer=movie_item.get("producer"),
                release_dt=int(movie_item.get("release_date")),
                rt_score=movie_item.get("rt_score"),
                url=movie_item.get("url"),
            )

            logger.info(
                msg=f"Successfully {'created' if created else 'updated'} a new Movie instance with PK: {instance}"
            )

            if isinstance(movie_item.get("people"), list):
                for person in movie_item.get("people"):
                    _person, created = Person.objects.update_or_create(
                        person_id=person.get("id"),
                        name=person.get("name"),
                        gender=PersonGenderChoices(person.get("gender").lower()).name,
                        age=person.get("age"),
                        eye_color=EyeColorChoices(person.get("eye_color").lower()).name,
                        hair_color=HairColorChoices(person.get("hair_color").lower()).name,
                    )

                    instance.people.add(_person)

            logger.info(msg="Successfully added Movies data.")

        send_statuses[__name__] = 200

        return send_statuses

    except Exception as exc:
        self.retry(exc=exc)
