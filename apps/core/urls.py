from django.urls import path

from apps.core.views import MoviesView

urlpatterns = [
    path("movies/", MoviesView.as_view(), name="movies"),
]
