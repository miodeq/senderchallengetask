from django.db import models
from django.utils import timezone

from apps.core.choices import PersonGenderChoices, EyeColorChoices, HairColorChoices


class BaseModel(models.Model):
    """
    Base model for all others holding creation and modification times, timezone specific.
    """

    dt_created = models.DateTimeField(verbose_name="Created", default=timezone.now)
    dt_modified = models.DateTimeField(verbose_name="Modified", auto_now=True)

    class Meta:
        abstract = True


class Person(BaseModel):
    """
    Represents people data.
    """

    person_id = models.UUIDField(verbose_name="ID")

    name = models.CharField(verbose_name="Name", max_length=100)
    gender = models.CharField(
        verbose_name="Gender", choices=PersonGenderChoices.choices(), max_length=10
    )
    age = models.CharField(verbose_name="Age", max_length=20)
    eye_color = models.CharField(
        verbose_name="Eye color", choices=EyeColorChoices.choices(), max_length=20
    )
    hair_color = models.CharField(
        verbose_name="Hair color", choices=HairColorChoices.choices(), max_length=20
    )

    def __str__(self):
        return str(self.person_id)

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"


class Movie(BaseModel):
    """
    Stores movie data.
    """

    movie_id = models.UUIDField(verbose_name="ID")

    title = models.CharField(verbose_name="Title", max_length=255)

    description = models.TextField(verbose_name="Description")

    director = models.CharField(verbose_name="Director", max_length=100)

    producer = models.CharField(verbose_name="Producer", max_length=100)

    release_dt = models.IntegerField(verbose_name="Release date", default=0)

    rt_score = models.PositiveSmallIntegerField(verbose_name="RT Score", default=0)

    people = models.ManyToManyField(to="core.Person")

    url = models.URLField(verbose_name="URL")

    def __str__(self):
        return str(self.movie_id)

    def get_movie_people(self):
        """
        Returns people assigned to given movie.
        """

        return (
            ", ".join([p.name for p in self.people.all()])
            if self.people.exists()
            else "No such data"
        )

    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"
