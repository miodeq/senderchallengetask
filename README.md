Sender Challenge
================

**Current stable version:** v1.0.0

**Release date:** 22.06.2020

### Author:
* Maciej Januszewski <maciek@mjanuszewski.pl>;

### Pre-requirements (example):
.env:
```bash
POSTGRES_USER=sender
POSTGRES_PASSWORD=Password1234
POSTGRES_DB=sender_challenge_db
SECRET_KEY=some_secret_key
DEBUG=1
DJANGO_PASSWORD=Admin1234
DJANGO_USERNAME=sender
REDIS_PASSWORD=some_redis_password
FLOWER_USER=sender
FLOWER_PASSWORD=Admin1234
```

### Task approaches:
```
In my opinion, there are multiple ways to handle this task and return expected results.

1) The first thing that came to my mind is just to use a Django's MemcachedCache (CACHES dict config with set timeout for 60 seconds):

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': REDIS_URL,
        'TIMEOUT': 60, # required time refresh from task requirements
    }
}

Then, once we have aggregated movies & cast data (for example a method which calls API and just returns the data):

merge_movie_with_people() # core/utils.py

We can return these data in our views.py using cache which will renew it in every 60 seconds:

from django.core.cache import cache

movies_data = cache.get('movies_data')

if not movies_data:
    movies_data = merge_movie_with_people()
    cache.set('movies_data', movies_data)

return render(request, "template.html", {"movies_data": movies_data})

2) But I thougt about more complex solution, because APIs sometimes fail or may not return the data for other reasons.

So, we want to keep the data in database. In given approach I used Celery as a messages broker and Redis to queue the tasks.

Every minute, collect_movies_data_task makes call to API, aggregates data and update database objects once one of the fields changed.

Also, I wanted to present packages and tools I used to work everyday, like: Nginx, Gunicorn, Redis, Flower, Docker, pre-commit etc.

The code is formatted (make black) and follows the PEP8 standards.

Code quality has been measured with pylint (make pylint) - 9.71/10.

Also, few basic tests have been added using pytest.
```

### Running app:
```bash
make runserver
```

### Movies data:
```
http://localhost:8000/movies/
```

### Admin panel:
```
sender / Admin1234 #entrypoint.sh
```
