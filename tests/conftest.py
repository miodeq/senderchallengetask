import os

import django
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sender_challenge.test_settings")


def pytest_configure():
    settings.DEBUG = True

    django.setup()
