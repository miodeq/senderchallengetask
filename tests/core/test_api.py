import pytest
import requests
from django.conf import settings
from django.test import TestCase

from apps.core.utils import merge_movie_with_people


class CoreTest(TestCase):
    """
    Core tests.
    """

    @pytest.fixture
    def sample_person_data(self):
        """
        Returns a sample person data from API.
        """

        return {
            "id": "ba924631-068e-4436-b6de-f3283fa848f0",
            "name": "Ashitaka",
            "gender": "Male",
            "age": "late teens",
            "eye_color": "Brown",
            "hair_color": "Brown",
            "films": ["https://ghibliapi.herokuapp.com/films/0440483e-ca0e-4120-8c50-4c8cd9b965d6"],
            "species": "https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2",
            "url": "https://ghibliapi.herokuapp.com/people/ba924631-068e-4436-b6de-f3283fa848f0",
        }

    @pytest.fixture
    def sample_movie_data(self):
        """
        Returns a sample movie data from API.
        """

        return {
            "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
            "title": "Castle in the Sky",
            "description": "The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa. With the help of resourceful Pazu and a rollicking band of sky pirates, she makes her way to the ruins of the once-great civilization. Sheeta and Pazu must outwit the evil Muska, who plans to use Laputa's science to make himself ruler of the world.",
            "director": "Hayao Miyazaki",
            "producer": "Isao Takahata",
            "release_date": "1986",
            "rt_score": "95",
            "people": ["https://ghibliapi.herokuapp.com/people/"],
            "species": [
                "https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2"
            ],
            "locations": ["https://ghibliapi.herokuapp.com/locations/"],
            "vehicles": ["https://ghibliapi.herokuapp.com/vehicles/"],
            "url": "https://ghibliapi.herokuapp.com/films/2baf70d1-42bb-4437-b551-e5fed5a87abe",
        }

    def test_success_ghibli_api_returns_data(self):
        """
        Test Case 1.1 - Verifying that API returns data.
        """

        self.assertIsNotNone(merge_movie_with_people())

    def test_films_api_endpoint(self):
        """
        Test Case 1.2 - Calls films API endpoint and verifies if response returns required keys.
        """

        response = requests.get(settings.GHIBLI_FILMS_ENDPOINT)

        self.assertEqual(first=response.status_code, second=200)

        expected_keys = [
            "id",
            "title",
            "description",
            "director",
            "producer",
            "release_date",
            "rt_score",
            "people",
            "species",
            "locations",
            "vehicles",
            "url",
        ]

        self.assertEqual(first=list(response.json()[0].keys()), second=expected_keys)

    def test_people_api_endpoint(self):
        """
        Test Case 1.3 - Calls people API endpoint and verifies if response returns required keys.
        """

        response = requests.get(settings.GHIBLI_PEOPLE_ENDPOINT)

        self.assertEqual(first=response.status_code, second=200)

        expected_keys = [
            "id",
            "name",
            "gender",
            "age",
            "eye_color",
            "hair_color",
            "films",
            "species",
            "url",
        ]

        self.assertEqual(first=list(response.json()[0].keys()), second=expected_keys)

    def test_movies_contains_people(self):
        """
        Test Case 1.4 - To verify if method properly find relations between movies and its cast.
        """

        pass
