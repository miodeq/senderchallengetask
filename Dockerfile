FROM python:3.8.0-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /src
WORKDIR /src

COPY poetry.lock /src
COPY pyproject.toml /src

RUN set -ex \
    && apk --update add --no-cache --virtual .build-deps \
       gcc \
       python3-dev \
       libffi-dev \
       libressl-dev \
       musl-dev \
       freetype-dev \
       zlib-dev \
    && apk --update add --no-cache --virtual \
        ca-certificates \
        make \
        postgresql-dev \
        bash \
    && pip install --no-cache-dir poetry \
    && poetry install --no-dev --no-interaction -vvv \
    && apk del --purge .build-deps \
    && rm -rf /var/cache/apk/*

COPY . /src

EXPOSE 8000
