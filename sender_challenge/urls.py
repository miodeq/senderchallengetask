# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

urlpatterns = [path("", admin.site.urls), path("", include("apps.core.urls"))]

urlpatterns += staticfiles_urlpatterns()

handler404 = "apps.core.views.handler404"
handler403 = "apps.core.views.handler403"
handler500 = "apps.core.views.handler500"
