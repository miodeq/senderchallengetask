import os

import dj_database_url
from celery.schedules import crontab

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = int(os.environ.get("DEBUG", False))

ALLOWED_HOSTS = ["*"]

DEFAULT_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
)

PROJECT_APPS = ("apps.core",)

THIRD_PARTY_APPS = ("django_celery_beat",)

INSTALLED_APPS = DEFAULT_APPS + PROJECT_APPS + THIRD_PARTY_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "sender_challenge.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "sender_challenge.wsgi.application"

DATABASE_URL = "postgres://{user}:{password}@postgres:5432/{db_name}".format(
    user=os.environ.get("POSTGRES_USER"),
    password=os.environ.get("POSTGRES_PASSWORD"),
    db_name=os.environ.get("POSTGRES_DB"),
)

DATABASES = {"default": dj_database_url.config(default=DATABASE_URL, conn_max_age=600)}

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Celery & Redis
REDIS_URL = f"redis://:{os.environ.get('REDIS_PASSWORD')}@redis:6379/0"

CELERY_BROKER_URL = REDIS_URL
CELERY_RESULT_BACKEND = REDIS_URL

CELERY_TIMEZONE = "Europe/Warsaw"

CELERY_IMPORTS = ("apps.core.tasks",)

CELERY_IGNORE_RESULT = True
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_ALWAYS_EAGER = False
CELERY_ACKS_LATE = True
CELERY_TASK_PUBLISH_RETRY = True
CELERY_DISABLE_RATE_LIMITS = False
CELERY_MESSAGE_COMPRESSION = "gzip"
CELERYD_POOL_RESTARTS = True
CELERYD_POOL = "gevent"

CELERYD_PREFETCH_MULTIPLIER = 1
CELERYD_MAX_TASKS_PER_CHILD = 4
CELERY_TASK_RESULT_EXPIRES = 600
CELERYD_HIJACK_ROOT_LOGGER = False
CELERYD_CONCURRENCY = 16

# GHIBLI API
GHIBLI_FILMS_ENDPOINT = "https://ghibliapi.herokuapp.com/films/"
GHIBLI_PEOPLE_ENDPOINT = "https://ghibliapi.herokuapp.com/people/"

CELERY_BEAT_SCHEDULE = {}

CELERY_BEAT_SCHEDULE_CORE = {
    # collect_movies_data_task
    "collect_movies_data_task": {"task": "collect_movies_data_task", "schedule": crontab(),},
}

CELERY_BEAT_SCHEDULE.update(CELERY_BEAT_SCHEDULE_CORE)
