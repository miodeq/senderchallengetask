#!/bin/sh

set -e

sleep 10

poetry run celery -A sender_challenge flower --port=80 --inspect-timeout=20000 --logLevel=info --url-prefix=flower --basic_auth=${FLOWER_USER}:${FLOWER_PASSWORD}

exec "$@"
