.PHONY: clean tests

clean:
	clear; rm -rf **/*.pyc \
    rm -rf ./.cache/ && \
    rm -rf .DS_Store && \
    rm -rf .pre-commit-venv && \
    rm -rf .pytest_cache && \
    rm -rf htmlcov && \
    rm -rf .coverage && \
    rm -rf .idea && \
    rm -rf *.png && \
    rm -rf media && \
    rm -f coverage.xml && \
    find . -name ".DS_Store" -print -delete && \
    find . -name "*.pyc" -exec rm -f {} \; && \
    find . -type d -name __pycache__ -exec rm -r {} \+

tests: clean
	poetry run pytest -s -v

setup-pre-commit:
	python3 -m venv .pre-commit-venv
	$(VENV)pip install --no-cache-dir poetry
	$(VENV)poetry install --no-interaction -vvv

tests-pre-commit: setup-pre-commit
	$(VENV)poetry run pytest -s -v

init:
	clear;poetry run pre-commit install

pylint: clean
	poetry run pylint -f colorized apps

black:
	clear;poetry run black --exclude="migrations" .

migrations:
	clear;poetry run python manage.py makemigrations --settings=sender_challenge.test_settings

runserver:
	clear; docker build . -t sender_challenge && docker-compose -f docker-compose.yml -p 'sender_challenge_local' up --remove-orphans
