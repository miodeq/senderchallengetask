#!/bin/sh

set -e

sleep 5

poetry run celery -P gevent -A sender_challenge.celery worker -l info -Q api_worker -n api_worker@%h -Ofair --autoscale=5,5 --maxtasksperchild=5

exec "$@"
